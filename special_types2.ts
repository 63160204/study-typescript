let w: unknown = 1;
w = "string";
w = {
    runANnoExistentMethod: () => {
        console.log("I think therefore I am");
    }
} as {runANnoExistentMethod: () => void }

if(typeof w == "object" && w != null){
    (w as {runANnoExistentMethod: () => void }).runANnoExistentMethod();

}